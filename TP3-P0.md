# TP3 : Docker

Dans ce TP, simple : Docker.

- [TP3 : Docker](#tp3--docker)
- [Prérequis](#prérequis)
- [I. Setup](#i-setup)
- [II. Premiers pas](#ii-premiers-pas)
  - [1. Conteneur NGINX](#1-conteneur-nginx)
  - [2. Une vraie application](#2-une-vraie-application)
- [III. Dockerfile](#iii-dockerfile)
- [IV. docker-compose](#iv-docker-compose)
  - [1. Premiers pas](#1-premiers-pas)
  - [2. NextCloud](#2-nextcloud)

# Prérequis

Dans ce TP, on va avoir besoin d'une seule machine, là encore, vous pouvez cloner une machine que vous avez déjà.

> Pour la conf réseau au sein de Rocky, j'ai écrit un mémo : [iciiii](../../cours/memo/rocky_network.md).

Vous déroulerez la **📝checklist📝** suivante, sur les deux machines :

- [x] la VM a un accès internet
  - `ping 1.1.1.1` fonctionnel
  - résolution de nom fonctionnelle : `ping cesi.fr` fonctionnel
  - IP = 10.2.1.50/24
- [x] SELinux est désactivé
  - la commande `sestatus` doit retourner : `Current Mode: permissive`
  - la commande `sestatus` doit retourner : `Mode from config file: permissive`
- [x] vous avez une connexion SSH fonctionnelle vers la VM
  - avec un échange de clé
- [x] la machine est nommée
  - fichier `/etc/hostname`
  - commande `hostname`

> La checklist ne doit pas figurer dans le rendu, elle doit simplement être effectuée sur toutes les machines.

# I. Setup

🌞 **Installez Docker en suivant [la doc officielle](https://docs.docker.com/)**

- Nettoyez les anciennes version de `docker`sur la machine

```bash
[toto@Docker1 ~]$  sudo yum remove docker \
>                   docker-client \
>                   docker-client-latest \
>                   docker-common \
>                   docker-latest \
>                   docker-latest-logrotate \
>                   docker-logrotate \
>                   docker-engine
No match for argument: docker
No match for argument: docker-client
No match for argument: docker-client-latest
No match for argument: docker-common
No match for argument: docker-latest
No match for argument: docker-latest-logrotate
No match for argument: docker-logrotate
No match for argument: docker-engine
No packages marked for removal.
Dependencies resolved.
Nothing to do.
Complete!
```
- Installation `docker` via repository

```bash
[toto@Docker1 ~]$ sudo yum install -y yum-utils
Last metadata expiration check: 1 day, 15:33:32 ago on Tue 07 Dec 2021 08:04:52 PM CET.
Dependencies resolved.
====================================================================================================================================================================
 Package                                 Architecture                         Version                                    Repository                            Size
====================================================================================================================================================================
Installing:
 yum-utils                               noarch                               4.0.21-3.el8                               baseos                                71 k

Transaction Summary
====================================================================================================================================================================
Install  1 Package

Total download size: 71 k
Installed size: 22 k
Downloading Packages:
yum-utils-4.0.21-3.el8.noarch.rpm                                                                                                   220 kB/s |  71 kB     00:00
--------------------------------------------------------------------------------------------------------------------------------------------------------------------
Total                                                                                                                                60 kB/s |  71 kB     00:01
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                                                                                            1/1
  Installing       : yum-utils-4.0.21-3.el8.noarch                                                                                                              1/1
  Running scriptlet: yum-utils-4.0.21-3.el8.noarch                                                                                                              1/1
  Verifying        : yum-utils-4.0.21-3.el8.noarch                                                                                                              1/1

Installed:
  yum-utils-4.0.21-3.el8.noarch

Complete!
[toto@Docker1 ~]$ sudo yum-config-manager \
>     --add-repo \
>     https://download.docker.com/linux/centos/docker-ce.repo
Adding repo from: https://download.docker.com/linux/centos/docker-ce.repo
```
- Facultatif : activez les référentiels de nuit ou de test.

```bash
[toto@Docker1 ~]$ sudo yum-config-manager --enable docker-ce-nightly
[toto@Docker1 ~]$ sudo yum-config-manager --enable docker-ce-test
[toto@Docker1 ~]$ sudo yum-config-manager --disable docker-ce-nightly
```

- Installez la dernière version de Docker Engine et de containerd, ou passez à l'étape suivante pour installer une version spécifique :

```bash
[toto@Docker1 ~]$ sudo yum install -y docker-ce docker-ce-cli containerd.io
Docker CE Stable - x86_64                                                                                                      84 kB/s |  19 kB     00:00
Docker CE Test - x86_64                                                                                                       118 kB/s |  22 kB     00:00
Dependencies resolved.
==============================================================================================================================================================
 Package                                    Architecture         Version                                                 Repository                      Size
==============================================================================================================================================================
Installing:
 containerd.io                              x86_64               1.4.12-3.1.el8                                          docker-ce-stable                28 M
 docker-ce                                  x86_64               3:20.10.11-3.el8                                        docker-ce-stable                22 M
 docker-ce-cli                              x86_64               1:20.10.11-3.el8                                        docker-ce-stable                29 M
Installing dependencies:
 checkpolicy                                x86_64               2.9-1.el8                                               baseos                         345 k
 container-selinux                          noarch               2:2.167.0-1.module+el8.5.0+710+4c471e88                 appstream                       53 k
 docker-ce-rootless-extras                  x86_64               20.10.11-3.el8                                          docker-ce-stable               4.6 M
 docker-scan-plugin                         x86_64               0.9.0-3.el8                                             docker-ce-stable               3.7 M
 fuse-common                                x86_64               3.2.1-12.el8                                            baseos                          20 k
 fuse-overlayfs                             x86_64               1.7.1-1.module+el8.5.0+710+4c471e88                     appstream                       71 k
 fuse3                                      x86_64               3.2.1-12.el8                                            baseos                          49 k
 fuse3-libs                                 x86_64               3.2.1-12.el8                                            baseos                          93 k
 libcgroup                                  x86_64               0.41-19.el8                                             baseos                          69 k
 libslirp                                   x86_64               4.4.0-1.module+el8.5.0+710+4c471e88                     appstream                       69 k
 policycoreutils-python-utils               noarch               2.9-16.el8                                              baseos                         251 k
 python3-audit                              x86_64               3.0-0.17.20191104git1c2f876.el8.1                       baseos                          85 k
 python3-libsemanage                        x86_64               2.9-6.el8                                               baseos                         126 k
 python3-policycoreutils                    noarch               2.9-16.el8                                              baseos                         2.2 M
 python3-setools                            x86_64               4.3.0-2.el8                                             baseos                         625 k
 slirp4netns                                x86_64               1.1.8-1.module+el8.5.0+710+4c471e88                     appstream                       50 k
Enabling module streams:
 container-tools                                                 rhel8

Transaction Summary
==============================================================================================================================================================
Install  19 Packages

Total download size: 92 M
Installed size: 382 M
Downloading Packages:
(1/19): container-selinux-2.167.0-1.module+el8.5.0+710+4c471e88.noarch.rpm                                                    349 kB/s |  53 kB     00:00
(2/19): libslirp-4.4.0-1.module+el8.5.0+710+4c471e88.x86_64.rpm                                                               444 kB/s |  69 kB     00:00
(3/19): fuse-overlayfs-1.7.1-1.module+el8.5.0+710+4c471e88.x86_64.rpm                                                         429 kB/s |  71 kB     00:00
(4/19): slirp4netns-1.1.8-1.module+el8.5.0+710+4c471e88.x86_64.rpm                                                            1.4 MB/s |  50 kB     00:00
(5/19): fuse-common-3.2.1-12.el8.x86_64.rpm                                                                                   239 kB/s |  20 kB     00:00
(6/19): fuse3-3.2.1-12.el8.x86_64.rpm                                                                                         508 kB/s |  49 kB     00:00
(7/19): fuse3-libs-3.2.1-12.el8.x86_64.rpm                                                                                    866 kB/s |  93 kB     00:00
(8/19): libcgroup-0.41-19.el8.x86_64.rpm                                                                                      806 kB/s |  69 kB     00:00
(9/19): checkpolicy-2.9-1.el8.x86_64.rpm                                                                                      1.4 MB/s | 345 kB     00:00
(10/19): python3-audit-3.0-0.17.20191104git1c2f876.el8.1.x86_64.rpm                                                           1.3 MB/s |  85 kB     00:00
(11/19): python3-libsemanage-2.9-6.el8.x86_64.rpm                                                                             1.9 MB/s | 126 kB     00:00
(12/19): policycoreutils-python-utils-2.9-16.el8.noarch.rpm                                                                   2.4 MB/s | 251 kB     00:00
(13/19): python3-setools-4.3.0-2.el8.x86_64.rpm                                                                               2.8 MB/s | 625 kB     00:00
(14/19): python3-policycoreutils-2.9-16.el8.noarch.rpm                                                                        3.7 MB/s | 2.2 MB     00:00
(15/19): docker-ce-20.10.11-3.el8.x86_64.rpm                                                                                  4.0 MB/s |  22 MB     00:05
(16/19): docker-ce-rootless-extras-20.10.11-3.el8.x86_64.rpm                                                                  4.0 MB/s | 4.6 MB     00:01
(17/19): docker-scan-plugin-0.9.0-3.el8.x86_64.rpm                                                                            3.4 MB/s | 3.7 MB     00:01
(18/19): docker-ce-cli-20.10.11-3.el8.x86_64.rpm                                                                              2.0 MB/s |  29 MB     00:14
(19/19): containerd.io-1.4.12-3.1.el8.x86_64.rpm                                                                              1.9 MB/s |  28 MB     00:15
--------------------------------------------------------------------------------------------------------------------------------------------------------------
Total                                                                                                                         5.8 MB/s |  92 MB     00:15
Docker CE Stable - x86_64                                                                                                      19 kB/s | 1.6 kB     00:00
Importing GPG key 0x621E9F35:
 Userid     : "Docker Release (CE rpm) <docker@docker.com>"
 Fingerprint: 060A 61C5 1B55 8A7F 742B 77AA C52F EB6B 621E 9F35
 From       : https://download.docker.com/linux/centos/gpg
Key imported successfully
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                                                                                      1/1
  Installing       : docker-scan-plugin-0.9.0-3.el8.x86_64                                                                                               1/19
  Running scriptlet: docker-scan-plugin-0.9.0-3.el8.x86_64                                                                                               1/19
  Installing       : docker-ce-cli-1:20.10.11-3.el8.x86_64                                                                                               2/19
  Running scriptlet: docker-ce-cli-1:20.10.11-3.el8.x86_64                                                                                               2/19
  Installing       : python3-setools-4.3.0-2.el8.x86_64                                                                                                  3/19
  Installing       : python3-libsemanage-2.9-6.el8.x86_64                                                                                                4/19
  Installing       : python3-audit-3.0-0.17.20191104git1c2f876.el8.1.x86_64                                                                              5/19
  Running scriptlet: libcgroup-0.41-19.el8.x86_64                                                                                                        6/19
  Installing       : libcgroup-0.41-19.el8.x86_64                                                                                                        6/19
  Running scriptlet: libcgroup-0.41-19.el8.x86_64                                                                                                        6/19
  Installing       : fuse3-libs-3.2.1-12.el8.x86_64                                                                                                      7/19
  Running scriptlet: fuse3-libs-3.2.1-12.el8.x86_64                                                                                                      7/19
  Installing       : fuse-common-3.2.1-12.el8.x86_64                                                                                                     8/19
  Installing       : fuse3-3.2.1-12.el8.x86_64                                                                                                           9/19
  Installing       : fuse-overlayfs-1.7.1-1.module+el8.5.0+710+4c471e88.x86_64                                                                          10/19
  Running scriptlet: fuse-overlayfs-1.7.1-1.module+el8.5.0+710+4c471e88.x86_64                                                                          10/19
  Installing       : checkpolicy-2.9-1.el8.x86_64                                                                                                       11/19
  Installing       : python3-policycoreutils-2.9-16.el8.noarch                                                                                          12/19
  Installing       : policycoreutils-python-utils-2.9-16.el8.noarch                                                                                     13/19
  Running scriptlet: container-selinux-2:2.167.0-1.module+el8.5.0+710+4c471e88.noarch                                                                   14/19
  Installing       : container-selinux-2:2.167.0-1.module+el8.5.0+710+4c471e88.noarch                                                                   14/19
  Running scriptlet: container-selinux-2:2.167.0-1.module+el8.5.0+710+4c471e88.noarch                                                                   14/19
  Installing       : containerd.io-1.4.12-3.1.el8.x86_64                                                                                                15/19
  Running scriptlet: containerd.io-1.4.12-3.1.el8.x86_64                                                                                                15/19
  Installing       : libslirp-4.4.0-1.module+el8.5.0+710+4c471e88.x86_64                                                                                16/19
  Installing       : slirp4netns-1.1.8-1.module+el8.5.0+710+4c471e88.x86_64                                                                             17/19
  Installing       : docker-ce-rootless-extras-20.10.11-3.el8.x86_64                                                                                    18/19
  Running scriptlet: docker-ce-rootless-extras-20.10.11-3.el8.x86_64                                                                                    18/19
  Installing       : docker-ce-3:20.10.11-3.el8.x86_64                                                                                                  19/19
  Running scriptlet: docker-ce-3:20.10.11-3.el8.x86_64                                                                                                  19/19
  Running scriptlet: container-selinux-2:2.167.0-1.module+el8.5.0+710+4c471e88.noarch                                                                   19/19
  Running scriptlet: docker-ce-3:20.10.11-3.el8.x86_64                                                                                                  19/19
  Verifying        : container-selinux-2:2.167.0-1.module+el8.5.0+710+4c471e88.noarch                                                                    1/19
  Verifying        : fuse-overlayfs-1.7.1-1.module+el8.5.0+710+4c471e88.x86_64                                                                           2/19
  Verifying        : libslirp-4.4.0-1.module+el8.5.0+710+4c471e88.x86_64                                                                                 3/19
  Verifying        : slirp4netns-1.1.8-1.module+el8.5.0+710+4c471e88.x86_64                                                                              4/19
  Verifying        : checkpolicy-2.9-1.el8.x86_64                                                                                                        5/19
  Verifying        : fuse-common-3.2.1-12.el8.x86_64                                                                                                     6/19
  Verifying        : fuse3-3.2.1-12.el8.x86_64                                                                                                           7/19
  Verifying        : fuse3-libs-3.2.1-12.el8.x86_64                                                                                                      8/19
  Verifying        : libcgroup-0.41-19.el8.x86_64                                                                                                        9/19
  Verifying        : policycoreutils-python-utils-2.9-16.el8.noarch                                                                                     10/19
  Verifying        : python3-audit-3.0-0.17.20191104git1c2f876.el8.1.x86_64                                                                             11/19
  Verifying        : python3-libsemanage-2.9-6.el8.x86_64                                                                                               12/19
  Verifying        : python3-policycoreutils-2.9-16.el8.noarch                                                                                          13/19
  Verifying        : python3-setools-4.3.0-2.el8.x86_64                                                                                                 14/19
  Verifying        : containerd.io-1.4.12-3.1.el8.x86_64                                                                                                15/19
  Verifying        : docker-ce-3:20.10.11-3.el8.x86_64                                                                                                  16/19
  Verifying        : docker-ce-cli-1:20.10.11-3.el8.x86_64                                                                                              17/19
  Verifying        : docker-ce-rootless-extras-20.10.11-3.el8.x86_64                                                                                    18/19
  Verifying        : docker-scan-plugin-0.9.0-3.el8.x86_64                                                                                              19/19

Installed:
  checkpolicy-2.9-1.el8.x86_64                                               container-selinux-2:2.167.0-1.module+el8.5.0+710+4c471e88.noarch
  containerd.io-1.4.12-3.1.el8.x86_64                                        docker-ce-3:20.10.11-3.el8.x86_64
  docker-ce-cli-1:20.10.11-3.el8.x86_64                                      docker-ce-rootless-extras-20.10.11-3.el8.x86_64
  docker-scan-plugin-0.9.0-3.el8.x86_64                                      fuse-common-3.2.1-12.el8.x86_64
  fuse-overlayfs-1.7.1-1.module+el8.5.0+710+4c471e88.x86_64                  fuse3-3.2.1-12.el8.x86_64
  fuse3-libs-3.2.1-12.el8.x86_64                                             libcgroup-0.41-19.el8.x86_64
  libslirp-4.4.0-1.module+el8.5.0+710+4c471e88.x86_64                        policycoreutils-python-utils-2.9-16.el8.noarch
  python3-audit-3.0-0.17.20191104git1c2f876.el8.1.x86_64                     python3-libsemanage-2.9-6.el8.x86_64
  python3-policycoreutils-2.9-16.el8.noarch                                  python3-setools-4.3.0-2.el8.x86_64
  slirp4netns-1.1.8-1.module+el8.5.0+710+4c471e88.x86_64

Complete!
```
- Start et test `Docker`.

```bash
[toto@Docker1 ~]$ sudo systemctl start docker
[toto@Docker1 ~]$ sudo docker run hello-world
Unable to find image 'hello-world:latest' locally
latest: Pulling from library/hello-world
2db29710123e: Pull complete
Digest: sha256:cc15c5b292d8525effc0f89cb299f1804f3a725c8d05e158653a563f15e4f685
Status: Downloaded newer image for hello-world:latest

Hello from Docker!
This message shows that your installation appears to be working correctly.

To generate this message, Docker took the following steps:
 1. The Docker client contacted the Docker daemon.
 2. The Docker daemon pulled the "hello-world" image from the Docker Hub.
    (amd64)
 3. The Docker daemon created a new container from that image which runs the
    executable that produces the output you are currently reading.
 4. The Docker daemon streamed that output to the Docker client, which sent it
    to your terminal.

To try something more ambitious, you can run an Ubuntu container with:
 $ docker run -it ubuntu bash

Share images, automate workflows, and more with a free Docker ID:
 https://hub.docker.com/

For more examples and ideas, visit:
 https://docs.docker.com/get-started/
```

🌞 **Setup Docker**

- ajoutez votre utilisateur au groupe `docker`

  - voir [la section dédiée à la gestion du user du mémo commande](../../cours/memo/commandes.md#gestion-dutilisateurs)
  - il faudra quitter puis réouvrir une session pour que cela prenne effet
```bash
[toto@Docker1 ~]$ sudo usermod -aG docker toto
[toto@Docker1 ~]$ groups toto
toto : toto wheel docker
```

- démarrez le service `docker`
- activez le service `docker` au démarrage de la machine
- vérifiez avec un `docker info` que tout est ok
  - vous devriez bah avoir une réponse et pas une erreur
```bash
[toto@Docker1 ~]$ sudo systemctl enable docker
[toto@Docker1 ~]$ sudo systemctl start docker
[toto@Docker1 ~]$ sudo docker info
[sudo] password for toto:
Client:
 Context:    default
 Debug Mode: false
 Plugins:
  app: Docker App (Docker Inc., v0.9.1-beta3)
  buildx: Build with BuildKit (Docker Inc., v0.6.3-docker)
  scan: Docker Scan (Docker Inc., v0.9.0)

Server:
 Containers: 1
  Running: 0
  Paused: 0
  Stopped: 1
 Images: 1
 Server Version: 20.10.11
 Storage Driver: overlay2
  Backing Filesystem: xfs
  Supports d_type: true
  Native Overlay Diff: true
  userxattr: false
 Logging Driver: json-file
 Cgroup Driver: cgroupfs
 Cgroup Version: 1
 Plugins:
  Volume: local
  Network: bridge host ipvlan macvlan null overlay
  Log: awslogs fluentd gcplogs gelf journald json-file local logentries splunk syslog
 Swarm: inactive
 Runtimes: io.containerd.runc.v2 io.containerd.runtime.v1.linux runc
 Default Runtime: runc
 Init Binary: docker-init
 containerd version: 7b11cfaabd73bb80907dd23182b9347b4245eb5d
 runc version: v1.0.2-0-g52b36a2
 init version: de40ad0
 Security Options:
  seccomp
   Profile: default
 Kernel Version: 4.18.0-348.2.1.el8_5.x86_64
 Operating System: Rocky Linux 8.5 (Green Obsidian)
 OSType: linux
 Architecture: x86_64
 CPUs: 1
 Total Memory: 1.748GiB
 Name: Docker1.tp3.cesi
 ID: PXH6:BEIE:UGBN:AXGD:RSV5:B6U5:BMI3:NKJD:EVHS:TLQX:XZUQ:PTSX
 Docker Root Dir: /var/lib/docker
 Debug Mode: false
 Registry: https://index.docker.io/v1/
 Labels:
 Experimental: false
 Insecure Registries:
  127.0.0.0/8
 Live Restore Enabled: false
```

🌸 **Enjoy**

- prenez le temps de jouer avec Docker
- lancez des trucs, testez des trucs
  - [petit mémo docker](../../cours/memo/docker.md) pour les commandes basiques
- regardez sur le Docker Hub s'il n'y a pas des conteneurs déjà tout faits d'applications que vous connaissez
  - j'sais po moi, juste des p'tits NGINX, Apache ou autres
  - ou des serveurs de jeu
  - ou des serveurs de wiki, de monitoring, de ticketing, etc
  - ou j'en sais rien moi, ce que vous voulez, essayez un peu des machins avant de passer à la suite !

- Essaie avec un serveur jeu Minecraft : 

Cette image de menu fixe fournit un serveur Minecraft qui téléchargera automatiquement la dernière version stable.
version au démarrage. Vous pouvez également exécuter / mettre à niveau vers une version spécifique ou le
dernier instantané. Voir le Les versions section ci-dessous pour plus d'informations.

- Pour utiliser simplement la dernière version stable, exécutez
```bash
[toto@Docker1 ~]$ sudo docker run -d -e EULA=TRUE -p 25565:25565 --name mc itzg/minecraft-server
Unable to find image 'itzg/minecraft-server:latest' locally
latest: Pulling from itzg/minecraft-server
7b1a6ab2e44d: Pull complete
8329695590e8: Pull complete
9bd6da4468db: Pull complete
8e07f21656cb: Pull complete
02686cfb3a15: Pull complete
9075fa25d30f: Pull complete
9d26506e3a23: Pull complete
59b792072e98: Pull complete
80227d0018dc: Pull complete
252bf0f5de60: Pull complete
011c343461be: Pull complete
f21063fb2a28: Pull complete
1a894d8d9b14: Pull complete
8fb2eda7525a: Pull complete
2e72321ea186: Pull complete
b8d8e7f2a4d0: Pull complete
9d3aea53b399: Pull complete
16cafa7a6287: Pull complete
98948ca80923: Pull complete
512be2c4bf42: Pull complete
cfb97e539e70: Pull complete
d3560a6c77e1: Pull complete
Digest: sha256:364748eeb4de06bf654dd3ef161ab05e639972df9f7fe8d17d0b54f9b6846bdd
Status: Downloaded newer image for itzg/minecraft-server:latest
391d65f15b95d9602e5f0a1bb9c2227a57cc61d46b4d2a513e32eb32be1e8c9b
[toto@Docker1 ~]$ sudo docker ps -a
CONTAINER ID   IMAGE                   COMMAND    CREATED         STATUS                   PORTS                                                      NAMES
65d528f6fc8b   itzg/minecraft-server   "/start"   6 minutes ago   Up 6 minutes (healthy)   0.0.0.0:25565->25565/tcp, :::25565->25565/tcp, 25575/tcp   mc
```
- Pour supprimer le `container`:

```bash
# -f pour FORCE la supression car le container est en status Up
[toto@Docker1 ~]$ sudo docker container rm -f 65d
```

# II. Premiers pas

## 1. Conteneur NGINX

🌞 **Lancez un conteneur NGINX**

- Téléchargement de nginx (suivant la version officielle sur https://hub.docker.com/_/nginx :
```bash
[toto@Docker1 ~]$ sudo docker pull nginx
Using default tag: latest
latest: Pulling from library/nginx
e5ae68f74026: Pull complete
21e0df283cd6: Pull complete
ed835de16acd: Pull complete
881ff011f1c9: Pull complete
77700c52c969: Pull complete
44be98c0fab6: Pull complete
Digest: sha256:9522864dd661dcadfd9958f9e0de192a1fdda2c162a35668ab6ac42b465f0603
Status: Downloaded newer image for nginx:latest
docker.io/library/nginx:latest
```
- Contenu du fichier de conf `nginx.conf` à créer dans /home/toto/nginx/ :
```nginx
events {
    multi_accept       on;
    worker_connections 65535;
}

http {
  server {
    listen 80;

    server_name docker.tp3.cesi;

    location / {
      root /var/www/html/cesi/;
      index index.html;
    }
  }
}
```
- NGINX doit servir une page HTML que vous avez créée vous-mêmes
  - et, pas de miracles hein, juste un `coucou` dans un fichier `index.html` ça ira
  - la racine web doit être `/var/www/html/cesi/`
  - on aura donc un fichier `/var/www/html/cesi/index.html`

- Créer le fichier `index.html` /home/toto/nginx/ :

```bash
<span style="text-decoration:blink;">COUCOU</span>
```

- Créer son conteneur :

```bash
[toto@docker ~]$ docker run -d -p 8080:80 -v /home/toto/nginx/index.html:/var/www/html/cesi/index.html -v /home/toto/nginx/nginx.conf:/etc/nginx/nginx.conf --name test1 nginx
# Mon port 8080 vers le port 80 du conteneur
# Mon fichier index.html dans /home/toto/gninx vers le dossier /var/www/html/cesi du conteneur,
# Mon fichier nginx.conf dans /home/toto/gninx vers le dossier /etc/nginx/nginx du conteneur,
[toto@docker ~]$ docker ps
CONTAINER ID   IMAGE     COMMAND                  CREATED          STATUS          PORTS                                   NAMES
6896a1483ddb   nginx     "/docker-entrypoint.…"   20 minutes ago   Up 20 minutes   0.0.0.0:8080->80/tcp, :::8080->80/tcp   test1
[toto@docker ~]docker exec -it test1 bash
root@1858d2513f63:/#
```


> Vous allez devoir utiliser un `-v` sur la ligne de commande `docker run` pour monter un fichier dans le conteneur (en l'occurence : deux `-v`, un pour la conf et un pour l'index HTML).

🌞 **Vous devez accéder à votre page HTML, depuis votre navigateur en tapant `http://web.tp3.cesi`**
- Si vous avez rajouter `http://web.tp3.cesi` dans le fichier `hosts` de la machine sinon il faut y accedez avec l'`ip` et le `port`

🌞 **Manipuler le conteneur qui tourne**

- Pour récupérer le terminal du conteneur :

```bash
[toto@docker nginx]$ docker exec -it test1 bash
```

- Déterminer l'adresse IP locale du conteneur :

```docker
[toto@docker nginx]$ docker inspect test1
[...]
            "IPv6Gateway": "",
            "MacAddress": "02:42:ac:11:00:02",
            "Networks": {
                "bridge": {
                    "IPAMConfig": null,
                    "Links": null,
                    "Aliases": null,
                    "NetworkID": "024298b34f3cdbbacbf5546b083d23314b00496e0f34e351a774fb8bd4b9a8e1",
                    "EndpointID": "adbd848adabcbf0006695cce6894a30930362b116bfec8ca0526efbb0f5182ed",
                    "Gateway": "172.17.0.1",
                    "IPAddress": "172.17.0.2",	<-------------
                    "IPPrefixLen": 16,
                    "IPv6Gateway": "",
                    "GlobalIPv6Address": "",
                    "GlobalIPv6PrefixLen": 0,
                    "MacAddress": "02:42:ac:11:00:02",
                    "DriverOpts": null
                }
            }
        }
    }
]
```

- Limiter la RAM à 128M dans un docker identique :

```bash
[toto@docker nginx]$ docker run -d -m 128M -p 8080:80 -v /home/toto/nginx/index.html:/var/www/html/cesi/index.html -v /home/toto/nginx/nginx.conf:/etc/nginx/nginx.conf --name test2 nginx

[toto@docker nginx]$ docker stats
CONTAINER ID   NAME      CPU %     MEM USAGE / LIMIT   MEM %     NET I/O          BLOCK I/O         PIDS
0aaca4d05ca8   test2     0.00%     28.58MiB / 128MiB   22.33%    2.9kB / 1.28kB   90.1kB / 8.19kB   2
```

## 2. Une vraie application

🌞 **Créer un réseau docker**

- avec une commande `docker network create <NAME>`
- vous l'appelerez `wiki`
- vous pouvez le voir et l'inspecter (`docker network ls` et `docker network inspect <NETWORK>`)

```bash
[toto@Docker1 ~]$ sudo docker network create wiki
9264fa1fa720c51fb7a2b9b8c00a8bd8d6f9097edf6664fe7eb42b065454f33d

[toto@Docker1 ~]$ sudo docker network ls
NETWORK ID     NAME      DRIVER    SCOPE
4ea1c112f606   bridge    bridge    local
30dd6bcb1953   host      host      local
939276a51fee   none      null      local
9264fa1fa720   wiki      bridge    local

[toto@Docker1 ~]$ sudo docker network inspect wiki
[
    {
        "Name": "wiki",
        "Id": "9264fa1fa720c51fb7a2b9b8c00a8bd8d6f9097edf6664fe7eb42b065454f33d",
        "Created": "2021-12-09T16:22:54.159810693+01:00",
        "Scope": "local",
        "Driver": "bridge",
        "EnableIPv6": false,
        "IPAM": {
            "Driver": "default",
            "Options": {},
            "Config": [
                {
                    "Subnet": "172.18.0.0/16",
                    "Gateway": "172.18.0.1"
                }
            ]
        },
        "Internal": false,
        "Attachable": false,
        "Ingress": false,
        "ConfigFrom": {
            "Network": ""
        },
        "ConfigOnly": false,
        "Containers": {},
        "Options": {},
        "Labels": {}
    }
]
```

🌞 **Lancez un conteneur MySQL**

- Télécharger le conteneur officiel mysql du site https://hub.docker.com/_/mysql :

```bash
[toto@Docker1 ~]$ sudo docker pull mysql
Using default tag: latest
latest: Pulling from library/mysql
ffbb094f4f9e: Pull complete
df186527fc46: Pull complete
fa362a6aa7bd: Pull complete
5af7cb1a200e: Pull complete
949da226cc6d: Pull complete
bce007079ee9: Pull complete
eab9f076e5a3: Pull complete
8a57a7529e8d: Pull complete
b1ccc6ed6fc7: Pull complete
b4af75e64169: Pull complete
3aed6a9cd681: Pull complete
23390142f76f: Pull complete
Digest: sha256:ff9a288d1ecf4397967989b5d1ec269f7d9042a46fc8bc2c3ae35458c1a26727
Status: Downloaded newer image for mysql:latest
docker.io/library/mysql:latest
```

- Lancer un conteneur mysql avec les critères suivants :
  
  - un mot de passe root défini
  - un user créé avec MDP
  - une base de données `wiki`
  - être dans le réseau `wiki`
  - être lancé en *daemon*
  - **avoir un nom** avec `--name`

```bash
[toto@docker nginx]$ docker run -d --name MySQL1 -e MYSQL_ROOT_PASSWORD=abc --network wiki -e MYSQL_USER=toto -e MYSQL_PASSWORD=mdptoto -e MYSQL_DATABASE=wiki mysql
a223178b7987df54f6ed7383e9f2d1672cfabac471c120e35596b534b8eed764
```
- Se connecter au conteneur et à la BDD en local :

```bash
[toto@Docker1 ~]$ sudo docker exec -it MySQL1 bash

root@a223178b7987:/# mysql -p

Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 8

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql>
```


🌞 **Lancez un conteneur [WikiJS](https://js.wiki/)**

- Télécharger le conteneur officiel wikijs du site https://hub.docker.com/r/requarks/wiki :

```bash
[toto@Docker1 ~]$ sudo docker pull requarks/wiki
Using default tag: latest
latest: Pulling from requarks/wiki
97518928ae5f: Pull complete
16a1a5057866: Pull complete
ad8b526cbb1f: Pull complete
0328e268dee9: Pull complete
21ca50003b2c: Pull complete
4f4fb700ef54: Pull complete
f613651fb5fa: Pull complete
8578e63ae298: Pull complete
b7ff63962f50: Pull complete
44a62cdd33ee: Pull complete
a00e0a631983: Pull complete
751305683829: Pull complete
881ff5c541c4: Pull complete
Digest: sha256:4d96ecb09ed6c3b2df483ba0c3b24e37df6ff1049171e6efcc4a1c0f39c2d502
Status: Downloaded newer image for requarks/wiki:latest
docker.io/requarks/wiki:latest
```

- Lancer un conteneur requarks/wiki avec les critères suivants :
  - être accessible sur un port de l'hôte
  - être dans le réseau `wiki`
  - utiliser la base lancée précédemment
  - être exposé sur l'hôte grâce à un partage de port
- tester que vous pouvez accéder à l'application depuis votre navigateur

```bash
[toto@Docker1 ~]$ sudo docker run -p 8880:80 -d --name wiki -e "DB_TYPE=mysql" -e "DB_HOST=MySQL1" -e "DB_PORT=3306" -e "DB_USER=toto" -e "DB_PASS=abc" -e "DB_NAME=wiki" requarks/wiki
fc473e30b3e8332849e5c167bf4d9a2ff1a707601ca3916e3716a8886316ccc6
```

> Y'a tout dans la doc officielle, la p'tite ligne `docker run` qui va bien.

# III. Dockerfile

Un `Dockerfile` est un fichier qui sert à construire une nouvelle image. Une fois le Dockerfile créé, on peut utilser la commande `docker build` afin de créer une image.

Le fichier est vraiment nommé comme ça, avec un majuscule, par convention.

> Voir le [mémo Docker](../../cours/memo/docker.md) pour un exemple de `Dockerfile` et de `docker build`.

🌞 **Créez une image**

- elle doit faire tourner notre ami `python3 -m http.server 8888`
  - pour rappel, ça lance un mini serveur web à l'aide de Python
- vous devez partir de [l'image Debian officielle](https://hub.docker.com/_/debian)
- le serveur Python doit rendre accessible le contenu de `/srv/test`
- le dossier `/srv/test/` doit contenir un fichier `index.html`

🌞 **Test**

- testez que vous accédez à l'application, depuis votre navigateur

# IV. docker-compose

## 1. Premiers pas

🌞 **Installez `docker-compose`** en suivant la doc officielle

> Là encore, vous pouvez suivre, sur Rocky, les instruction données pour CentOS.

---

🌞 **Lancez un `docker-compose.yml` de test**

- la commande c'est `docker-compose up`
  - la commande ne peut être lancée que s'il y a un `docker-compose.yml dans le dossier courant
- contenu du fichier `docker-compose.yml` :

```yml
version: "3.5"

services:
  web:
    nginx:
      image: nginx
      ports: 8080:80
      networks:
        test-net:

networks:
  test-net:
```

Créez un nouveau dossier, dans votre homedir par exemple, et créez un nouveau fichier `docker-compose.yml` pour y déposer le contenu ci-dessus. Ce sera votre répertoire de travail.  

**Vous n'aurez accès aux commandes `docker-compose` que si vous vous trouvez dans un répertoire qui contient un fichier `docker-compose.yml`.**  

> Il en va de la bonne pratique de dédier entièrement ce dossier à l'application mise en place par le fichier `docker-compose.yml`. Et donc, de ne stocker d'autres fichiers que s'ils sont nécessaires au bon fonctionnement de l'application.

🌞 **Vérifier**

- que ça tourne avec un `docker ps`
- `docker-compose ps` est aussi dispo
- visitez le site depuis votre navigateur

---

Manipulons un peu le fichier `docker-compose.yml` ! 

🌞 **Dans le même fichier**

- ajouter un conteneur debian
- il doit lancer la commande `sleep 99999` au démarrage
- il doit être dans le même réseau

🌞 **Lancez le `docker-compose.yml` modifié**

- récupérez un shell dans le conteneur debian
- vérifiez que vous pouvez ping l'autre conteneur en utilisant son nom (pas son IP) : `ping <NOM>`

---

L'utilisation de variables permettra d'éviter de répéter plusieurs fois la même valeur dans notre `docker-compose.yml`, en plus d'en faciliter la modification.

🌞 **Variables**

- créez dans le même dossier que le fichier `docker-compose.yml` un fichier `.env`
  - à l'intérieur, vous pouvez définir des variables sous la forme `VAR=VALEUR`
  - ces variables sont accessibles dans le `docker-compose.yml`
- définissez une variable `NGINX_PORT` à la valeur `8080`
- utilisez la variable dans le fichier pour exposer le port de NGINX

## 2. NextCloud

🌞 **Créez un `docker-compose.yml`**

- il lance 3 conteneurs
  - un conteneur NextCloud
    - il existe un conteneur édité par NextCloud sur le hub
  - un conteneur MySQL
    - image officielle
  - un conteneur NGINX
    - image officielle
- le conteneur NGINX
  - doit proxy vers NextCloud
  - doit permettre l'établissement de connexions HTTPS
  - doit être exposé via un partage de port sur l'hôte
- l'application doit être joignable sur le nom `nextcloud.tp3.cesi`
- comme dans l'exemple un peu plus bas, le fichier utilise un maximum de variable

> Allez-y étape par étape. Lancez déjà NextCloud et sa base de données avec `docker-compose`. Ajoutez le proxying. Puis enfin le proxying en HTTPS.  
N'oubliez pas que les conteneurs se joignent naturellement *via* leurs noms s'ils sont dans le même réseau.

Créez un répertoire dédié à travailler sur ce `docker-compose.yml`. Le mien ressemblait à ça à la fin :

```bash
.
├── data/                   # monté dans le conteneur NextCloud
├── db/                     # monté dans le conteneur MySQl
├── docker-compose.yml
└── nginx/
    ├── nginx.conf          # monté dans le conteneur NGINX
    ├── web.tp3.cesi.crt    # monté dans le conteneur NGINX
    └── web.tp3.cesi.key    # monté dans le conteneur NGINX
```

Il est possible de donner plusieurs noms aux conteneurs avec cette syntaxe (les `aliases`) :

```bash
nextcloud_app:
    image: nextcloud
    restart: always
    volumes:
      - ./data:/var/www/html
    environment:
      - MYSQL_HOST=nextcloud_db
      - MYSQL_DATABASE=$NEXTCLOUD_MYSQL_DATABASE
      - MYSQL_USER=$NEXTCLOUD_MYSQL_USER
      - MYSQL_PASSWORD=$NEXTCLOUD_MYSQL_PASSWORD
    networks:
      nextcloud:
        aliases:
          - web.tp3.cesi
```

> Pour le détail de la génération des certificats ou de la configuration du proxy, référez-vous à [la partie 2 du TP2](../2/part2.md#1-reverse-proxy)

---
